package taringudMang;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.LineBorder;

//Minu keele oskus on nork, sest ma teen palju vigu :)

public class Game {

	public static void main(String[] args) {
		new Game();
	};
	//avalik muutujad
	int youRand, youRand2, systemRand, systemRand2, summarOwner, summarSystem, x, y, bet;
	int bank = 10;
	// initsialisatsioon
	Frame frame = new Frame();
	Button nupp = new Button("Roll");
	Button nupp2 = new Button("Raise your bet");
	Button nupp3 = new Button("Downgrade your bet");
	Button nupp4 = new Button("All in!");
	Button nupp5 = new Button("Get some credit");
	Panel panel = new Panel();
	Panel panel2 = new Panel();
	Panel panel3 = new Panel();
	Label lbl = new Label();
	Label lbl3 = new Label();
	ImageIcon[] image1 = new ImageIcon[7];
	ImageIcon[] image2 = new ImageIcon[7];
	JLabel lblCenter = new JLabel();
	JLabel lblCenter2 = new JLabel();
	WindowListener wl = new WindowAdapter() { // Ledsin seda foorumites, et aken saab kinni panema
		public void windowClosing(WindowEvent e) {
			System.exit(0);
		}
	};
	// Nuppude kontroller. Vastab kuidas tootab nuppud (Pohiosa)
	ActionListener al = new ActionListener() {
		public void actionPerformed(ActionEvent e) {

			if (e.getSource() == nupp) { // "Roll" nupp tegevused.
				pohiosa();
				bank();
				lblCenter.setIcon(image1[youRand]);
				lblCenter2.setIcon(image2[youRand2]);

				if (summarOwner > summarSystem) {
					bank += bet * 2;
				}
				if (summarOwner < summarSystem) {
					bank -= bet;
				}
				lbl.setText("Now, you have: " + bank + "$");

			}
			if (e.getSource() == nupp2) {// "Raise bet" nupp tegevus.
				raiseBet();
				txtrNowBetIs.setText("Now bet is: " + bet);

			}
			if (e.getSource() == nupp3) {// "Downgrade bet" nupp tegevus.
				downgradeBet();
				txtrNowBetIs.setText("Now bet is: " + bet);

			}
			if (e.getSource() == nupp4) {// "All-in" nupp tegevus.
				allin();
				txtrNowBetIs.setText("Now bet is: " + bet);
			}
			if (e.getSource() == nupp5) {// "Get some credit" nupp tegevus.
				bank += 30;
				lbl.setText("Now, you have: " + bank + "$");
				nupp.setEnabled(true); // teeb et nupp "Roll" hakka tootama
			}
		}
	};
	private final JLabel txtrNowBetIs = new JLabel();

	public void pohiosa() {
		youRand = randomarv(1, 6); // Muutujad mis osalevad programmis
		// youRand = Math.round(youRand);//
		youRand2 = randomarv(1, 6);
		// youRand2 = Math.round(youRand2);
		systemRand = randomarv(1, 6);
		// systemRand = Math.round(systemRand);
		systemRand2 = randomarv(1, 6);
		// systemRand2 = Math.round(systemRand2);
		summarOwner = youRand + youRand2;
		summarSystem = systemRand + systemRand2;
		
		// programm kus programm teab kas sina on winner voi loser :)
		if (summarOwner > summarSystem) {
			lbl3.setText("Win");
		} else {
			lbl3.setText("Lose");
		}
		if (summarOwner == summarSystem) {
			lbl3.setText("Draw");
		}
	};

	// Random method(oli praktikumis)
	public int randomarv(int min, int max) {
		int vahemik = max - min + 1;
		return (int) (Math.random() * vahemik) + min;
	}

	// Metood mis suurendab panuse.
	public void raiseBet() {
		bet += 5;
		if (bet > bank) {
			bet = bank;
		}
	}

	// Metood mis vahendab panuse.

	public void downgradeBet() {
		bet -= 5;
		if (bet < 0) {
			bet = +0;
		}
	}

	// Meetod mis paneb koik raha.
	public void allin() {
		bet = bank;
	}

	// Meetod mis raagib, et ei ole raha.
	public void bank() {
		if (bank < 0) {
			nupp.setEnabled(false);
			lbl3.setText("You lost all your money ! :( You can't play anymore");
		}
	}
	
	public void imageAdd() {
		for (x = 0; x < 7; x++) {
			y = x;

			image1[x] = new ImageIcon(y + ".gif");
			image2[x] = new ImageIcon(y + ".gif");
		}
	}

	// Displays the components
	public Game() {
		imageAdd();

		// Frame

		frame.setMinimumSize(new Dimension(320, 240));
		frame.setLayout(new BorderLayout());
		frame.add(panel, BorderLayout.NORTH);
		frame.add(panel3, BorderLayout.CENTER);
		frame.add(panel2, BorderLayout.WEST);

		// Panel

		panel.setLayout(new GridLayout(2, 1));
		panel2.setLayout(new GridLayout(5, 1));
		panel.add(lbl);
		panel.add(lbl3);
		panel2.add(nupp);
		panel2.add(nupp2);
		panel2.add(nupp3);
		panel2.add(nupp4);
		panel2.add(nupp5);
		panel3.add(lblCenter);
		panel3.add(lblCenter2);
		panel3.add(txtrNowBetIs);
		panel3.setLayout(new GridLayout(1, 2));
		panel3.setBackground(new Color(127, 255, 212));// backround color (aquamarine)
		panel2.setBackground(new Color(10, 205, 0));
		panel.setBackground(new Color(127, 255, 212));
		// Labels <-|

		lblCenter.setHorizontalAlignment(SwingConstants.CENTER);
		lblCenter.setVerticalTextPosition(SwingConstants.TOP);
		lblCenter.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblCenter.setHorizontalTextPosition(SwingConstants.CENTER);
		lblCenter.setText("Sinu kuubikud");
		lblCenter.setIcon(image1[1]);

		// Label2 |->

		lblCenter2.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblCenter2.setHorizontalAlignment(SwingConstants.CENTER);
		lblCenter2.setHorizontalTextPosition(SwingConstants.CENTER);
		lblCenter2.setVerticalTextPosition(SwingConstants.TOP);
		lblCenter2.setName("");
		lblCenter2.setText("Systemi kuubikud");
		lblCenter2.setIcon(image2[1]);
		lbl.setBackground(new Color(127, 255, 212));
		lbl3.setBackground(new Color(127, 255, 212));
		lbl.setText("Now, you have: " + bank + "$");

		// TextField

		txtrNowBetIs.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		txtrNowBetIs.setBorder(new LineBorder(new Color(0, 0, 0)));
		txtrNowBetIs.setFont(new Font("Mongolian Baiti", Font.PLAIN, 17));
		txtrNowBetIs.setText("Now bet is: " + bet);

		// Nuppid
		nupp.addActionListener(al);
		nupp2.addActionListener(al);
		nupp3.addActionListener(al);
		nupp4.addActionListener(al);
		nupp5.addActionListener(al);

		frame.addWindowListener(wl);
		frame.pack();
		frame.setVisible(true);

	}
}